import matplotlib.pyplot as plt
import csv

from numpy import empty

def use_bar():
    """Bar example."""
    x = []
    y = []

    with open('data/biostats.csv', 'r') as csvfile:
        plots = csv.reader(csvfile, delimiter=',')

        skipFirst = True

        for row in plots:
            if skipFirst or row == []:
                skipFirst = False
                continue
            x.append(row[0])
            y.append(int(row[2]))
        
    plt.bar(x, y, color = 'g', width = 0.72, label = "Age") 
    plt.xlabel('Names') 
    plt.ylabel('Ages') 
    plt.title('Ages of different persons') 
    plt.legend() 
    plt.show() 
    pass

def use_plot():
    """Plot example."""
    x = []
    y = []

    with open('data/Weatherdata.csv', 'r') as csvfile:
        plots = csv.reader(csvfile, delimiter=',')

        skipFirst = True

        for row in plots:
            if skipFirst or row == []:
                skipFirst = False
                continue
            x.append(row[0])
            y.append(int(row[1]))
        
    plt.plot(x, y, color = 'g', linestyle = 'dashed', marker = 'o', label = 'Weather Data')
    plt.xticks(rotation = 25) 
    plt.xlabel('Dates')
    plt.ylabel('Temperature(°C)') 
    plt.title('Weather Report', fontsize = 20) 
    plt.grid()
    plt.legend()
    plt.show()
    pass

if __name__ == '__main__':
    #use_bar()
    use_plot()
