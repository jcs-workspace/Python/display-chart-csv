# display-chart-csv

Learn to display csv in chart using `Python`.

- [Visualize data from CSV file in Python](https://www.geeksforgeeks.org/visualize-data-from-csv-file-in-python/)
- [CSV Files from Florida State University](https://people.sc.fsu.edu/~jburkardt/data/csv/csv.html)

Using the library [matplotlib](https://matplotlib.org/) --- Visualization with Python.
